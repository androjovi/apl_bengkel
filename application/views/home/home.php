<?php $this->load->view('front/header.php'); ?>
<?php $this->load->view('front/navbar.php'); ?>
<div class="container">
  <br><br>

<div class="row">
    <div class="col s12 m12 l12 xl12">
      <form action="javascript:void(0)" method="post" id="form_barang">
      <div class="row">
        <div class="input-field col s12 m12 l12 xl12">
          <i class="material-icons prefix">shopping_cart</i>
          <input type="text" name="nama" id="autocomplete-input" class="autocomplete nama">
          <label for="autocomplete-input">Nama penjualan</label>
        </div>
        <div class="input-field col s12 m12 l12 xl12">
          <i class="material-icons prefix">attach_money</i>
          <input type="number" name="harga" id="autocomplete-harga" class="autocomplete harga">
          <label for="autocomplete-harga">Harga barang</label>
        </div>
        <div class="input-field col s12 m12 l12 xl12 center">
          <button class="btn" id="submit">Masukkan</button>

        </div>
      </div>
    </div>
    <div class="col s12">
      <blockquote>
      Total penjualan hari ini : <br><b>Rp <?php echo num_format($total->harga_barang) ?></b>
    </blockquote>
    </div>
  </div>
</div>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.22.0/sweetalert2.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.22.0/sweetalert2.min.js"></script>
<script>
$(document).ready(function(){
$("#submit").click(function(){
  $.ajax({
    url : "<?php echo site_url('home/proses_input') ?>",
    type: 'post',
    data : $("#form_barang").serialize(),
    beforeSend: function(){
       $("#submit").hide();
   },
    success : function(response){
      swal({
            type: 'success',
            title: 'Berhasil memasukkan data',
            showConfirmButton: true,
            timer: 4000
        })
        $("#submit").show();
  }
})
return false;
})
})
</script>
<?php $this->load->view('front/js.php'); ?>
