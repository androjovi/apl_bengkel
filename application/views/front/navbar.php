<nav>
   <div class="nav-wrapper">
     <a href="#!" class="brand-logo">Logo</a>
     <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
     <ul class="right hide-on-med-and-down">
       <li><a href="<?php echo base_url('/') ?>">Input pemasukan</a></li>
       <li><a href="badges.html">List barang</a></li>
       <li><a href="<?php echo base_url('/report') ?>">Report</a></li>
     </ul>
   </div>
 </nav>

 <ul class="sidenav" id="mobile-demo">
   <li><a href="<?php echo base_url('/') ?>">Input pemasukan</a></li>
   <li><a href="badges.html">List barang</a></li>
   <li><a href="<?php echo site_url('/report') ?>">Report</a></li>
 </ul>
