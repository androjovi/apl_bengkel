<?php $this->load->view('front/header.php'); ?>
<?php $this->load->view('front/navbar.php'); ?>
<div class="container">
  <br><br>

<div class="row">

  <form action="javascript:void(0)" method="post" id="pickers">
  <div class="col s6 m6 l6 xl6">
  <p>Dari tanggal: <input type="text" name="dari_tanggal" id="datepicker1" class="datepicker"></p>
</div>
<div class="col s6 m6 l6 xl6">
<p>Sampai tanggal: <input type="text" name="sampai_tanggal" id="datepicker2" class="datepicker"></p>
</div>
<div class="col s12 m12 l12 xl12 center">
  <button id="submit" class="waves-effect waves-light btn"><i class="material-icons left">search</i>cari</button>
</div>
</div>
  </div>
  <div class="container">
  <div class="col s12 m12 l12 xl12">
    <table class="responsive-table highlight">
        <thead>
          <tr>
              <th>Barang</th>
              <th>Harga</th>
              <th>Jam</th>
              <th>Waktu</th>
          </tr>
        </thead>
        <tbody id="parse_data">

        </tbody>
      </table>
  </div>
</div>
</div>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.22.0/sweetalert2.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.22.0/sweetalert2.min.js"></script>
<script>
$(document).ready(function(){
  $( function() {
    $( "#datepicker1" ).datepicker({
      format : 'yyyy-mm-dd',
      showClearBtn : true,
    });
    $( "#datepicker2" ).datepicker({
      format : 'yyyy-mm-dd',
      showClearBtn : true,
    });
  } );
})
$("#submit").click(function(){
  $.ajax({
    url : "<?php echo site_url('report/get_data'); ?>",

    type: 'post',
    data : {dari_tanggal : $("#datepicker1").val(),
            sampai_tanggal : $("#datepicker2").val()
           },
           dataType: "json",
    success: function(response){
      $.each(response ,function(i, item){
        $("#parse_data").append(
          "<tr><td>"+item.nama_penjualan+"</td><td>Rp "+item.harga_barang+"</td><td>"+item.jam+"</td><td>"+item.waktu+"</td></tr>"
        )
      })
    },
    error: function(xml, error) {
    console.log(error);
  }
})
$("#parse_data").empty();
})
function get_l(){

}
</script>
<?php $this->load->view('front/js.php'); ?>
