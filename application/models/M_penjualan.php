<?php

class M_penjualan extends CI_Model{
  function get_total(){
    $day = date('Y-m-d');
    $this->db->select_sum('harga_barang');
    $this->db->where('waktu_tanggal REGEXP ', "^$day");
    return $this->db->get('penjualan');

  }
}
