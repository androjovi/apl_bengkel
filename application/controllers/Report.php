<?php
class Report extends CI_Controller{
  function __construct(){
    parent::__construct();
      $this->load->model('m_report');
  }
  function index(){
    $this->load->view('report/report');
  }
  function get_data(){

    $t = $this->m_report->get_between($this->input->post('dari_tanggal'), $this->input->post('sampai_tanggal'));
    foreach ($t->result() as $k){
      $r = explode(" ", $k->waktu_tanggal);
      $arr[] = array(
        'id'             => $k->id,
        'nama_penjualan' => $k->nama_penjualan,
        'harga_barang'   => num_format($k->harga_barang),
        'jam'            => $r[1],
        'waktu'          => $r[0]
      );
    }
    echo json_encode($arr);
  }
}
