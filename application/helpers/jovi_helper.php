<?php

function get_autocomplete_nama(){
  $CI = &get_instance();

  $get = $CI->db->get('penjualan');
  foreach ($get->result() as $k){
    $arr = array(
      $k->nama_penjualan => null
    );
  }
  echo json_encode($arr);
}
function num_format($angka){
  return number_format($angka,0,".",","); // format penulisan di indonesia, klo ndak salah sih
}
function sum_total(){
  $CI = &get_instance();
  $CI->db->select_sum('harga_barang');
  $f = $CI->db->get('penjualan')->result();
  foreach($f as $k){
    return $k->harga_barang;
  }
}
